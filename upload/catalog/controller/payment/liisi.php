<?php
class ControllerPaymentLiisi extends Controller {
  private $data;
  private $orderStatus = array("complete"=>"5", "cancel"=>7, "failed"=>10);

  public function index() {
    $this->language->load('payment/liisi');
    $this->data['button_confirm'] = $this->language->get('button_confirm');
    $url = array("test"=>"https://prelive.liisi.ee/api/ipizza/", "live"=>"https://klient.liisi.ee/api/ipizza/");
    $server = ($this->config->get('liisi_server') == "1" ? "live" : "test");
    $this->data['action'] = $url[$server];
  
    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

    if ($order_info) {
      $this->data['formFields'] = $this->getFormFields($order_info);
      return $this->load->view('default/template/payment/liisi.tpl', $this->data);
    }
  }

  private function getFormFields($order)
  {
    $VK['VK_SERVICE'] = "1012";
    $VK['VK_VERSION'] = "008";
    $VK['VK_SND_ID'] = $this->config->get('liisi_snd_id');
    $VK['VK_STAMP'] = $order['order_id'];
    $VK['VK_AMOUNT'] = $this->currency->format((float)$order['total'], "EUR", '', false);
    $VK['VK_CURR'] = "EUR";
    $VK['VK_REF'] = '';
    $VK['VK_MSG'] = 'Order '. $order['order_id'];
    $VK['VK_RETURN'] = $this->url->link('payment/liisi/callback', '', 'SSL');
    $VK['VK_CANCEL'] = $VK['VK_RETURN'];

    $datetime = new DateTime();
    $VK['VK_DATETIME'] = $datetime->format(DateTime::ISO8601);

    $VK['VK_MAC'] = $this->generateMAC($VK);
    $VK['VK_ENCODING'] = "UTF-8";
    $VK['VK_LANG'] = "EST";
    return $VK;
  }

  /**
   * Generating MAC code
   * @param   Array   $source data for MAC calculating
   * @return  string      MAC code
   */
  private function generateMAC(Array $source)
  {
    $data = '';
    foreach($source as $label){
      $data .= $this->padding($label) . $label;
    }
    $privKey = openssl_get_privatekey(
      $this->config->get('liisi_private'),
      $this->config->get('liisi_private_password')
    );
    if (!$privKey)
    { 
      echo '<b style="color:#ff0000;">There is an error with Liisi Payment module private key. Please contact the servers administrator.</b><br>';
      return 0;
    }
    openssl_sign($data, $signature, $privKey);
    $VK_MAC = base64_encode($signature);
    openssl_free_key($privKey);
    return $VK_MAC;
  }

  private function padding($string)
  {
    return str_pad(strlen($string), 3, '0', STR_PAD_LEFT);
  }
  
  public function callback() {
    if (isset($this->request->post['VK_STAMP'])) {
      $order_id = trim($this->request->post['VK_STAMP']);
    } else {
      die('Illegal Access');
    }
  
    $this->load->model('checkout/order');
    $order = $this->model_checkout_order->getOrder($order_id);
    if ($order) {
      $data = array_merge($this->request->post,$this->request->get);
  
      $verification = $this->vertifySign('VK_MAC');
      $verification = 1; // For testing
      if($order['order_id'] && $verification == 0)
      {
        $this->model_checkout_order->addOrderHistory($order['order_id'], $this->orderStatus['failed']);
        die('Invalid public key');
      }
      else if ($order['order_id'] && $verification == 1)
      {     
        if($data['VK_SERVICE'] == '1111')
        {
           if (isset($order['order_id'])) {
            $this->model_checkout_order->addOrderHistory($order['order_id'], $this->orderStatus['complete']);
            $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
            die("Order completed");
          }
        }
        else if ($data['VK_SERVICE'] == '1911')
        {
          $this->model_checkout_order->addOrderHistory($order['order_id'], $this->orderStatus['cancel']);
          $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));
          die('Cancelled');
        }
        else
        {
          $this->model_checkout_order->addOrderHistory($order['order_id'], $this->orderStatus['failed']);
          $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));
          die('We have encountered a problem processing your payment, please contact the system administrator.');
        }   
      }
      else
      {
        $this->redirect($this->url->link('', ''));
      }
    }
  }

  public function vertifySign($key) 
  {
    $data = array_merge($this->request->post,$this->request->get);
    if (isset($data[$key]))
    {
      $signature = base64_decode($data[$key]);  
    }
    else
    {
      return 2;
    }

    $data = $this->getResponseFields();
    $cert = $this->config->get('liisi_public');
    $publicKey = openssl_get_publickey($cert);
    $out = openssl_verify($data, $signature, $publicKey);
    openssl_free_key($publicKey);
    return $out;
  }

  private function getResponseFields()
  {
      $data = array_merge($this->request->post,$this->request->get);
      if(isset($data['VK_SERVICE']))
      {
        $array = array(
          'VK_SERVICE' => $data['VK_SERVICE'],
          'VK_VERSION' => $data['VK_VERSION'],
          'VK_SND_ID' => $data['VK_SND_ID'],
          'VK_REC_ID' => $data['VK_REC_ID'],
          'VK_STAMP' => $data['VK_STAMP']
        );

        switch($data['VK_SERVICE'])
        {
          case '1111':
            $array['VK_T_NO'] = $data['VK_T_NO'];
            $array['VK_AMOUNT'] = $data['VK_AMOUNT'];
            $array['VK_CURR'] = $data['VK_CURR'];
            $array['VK_REC_ACC'] = $data['VK_REC_ACC'];
            $array['VK_REC_NAME'] = $data['VK_REC_NAME'];
            $array['VK_SND_ACC'] = $data['VK_SND_ACC'];
            $array['VK_SND_NAME'] = $data['VK_SND_NAME'];
            $array['VK_REF'] = $data['VK_REF'];
            $array['VK_MSG'] = $data['VK_MSG'];
            $array['VK_T_DATETIME'] = $data['VK_T_DATETIME'];
            break;
          case '1911':
            $array['VK_REF'] = $data['VK_REF'];
            $array['VK_MSG'] = $data['VK_MSG'];
            break;
        }
      }
      
      $data = '';
      foreach($array as $label)
      {
        $data .= $this->padding($label).$label;
      }

      return $data;
  }
}
?>
