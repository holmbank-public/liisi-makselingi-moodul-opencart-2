<?php
class ControllerPaymentLiisi extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/liisi');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('liisi', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('payment/liisi', 'token=' . $this->session->data['token'], true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['entry_field_status'] = $this->language->get('entry_field_status');
		$data['entry_field_server'] = $this->language->get('entry_field_server');
		$data['entry_field_snd'] = $this->language->get('entry_field_snd');
		$data['entry_field_public'] = $this->language->get('entry_field_public');
		$data['entry_field_private'] = $this->language->get('entry_field_private');
		$data['entry_field_private_password'] = $this->language->get('entry_field_private_password');
		$data['entry_field_payment_confirm'] = $this->language->get('entry_field_payment_confirm');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (isset($this->error['bank' . $language['language_id']])) {
				$data['error_bank' . $language['language_id']] = $this->error['bank' . $language['language_id']];
			} else {
				$data['error_bank' . $language['language_id']] = '';
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('payment', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/liisi', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('payment/liisi', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('payment', 'token=' . $this->session->data['token'], true);

		$this->load->model('localisation/language');

		foreach ($languages as $language) {
			if (isset($this->request->post['liisi_bank' . $language['language_id']])) {
				$data['liisi_bank' . $language['language_id']] = $this->request->post['liisi_bank' . $language['language_id']];
			} else {
				$data['liisi_bank' . $language['language_id']] = $this->config->get('liisi_bank' . $language['language_id']);
			}
		}

		$data['languages'] = $languages;

		$data['liisi_server'] = (isset($this->request->post['liisi_server']) ? $this->request->post['liisi_server'] : $this->config->get('liisi_server'));
		$data['liisi_public'] = (isset($this->request->post['liisi_public']) ? $this->request->post['liisi_public'] : $this->config->get('liisi_public'));
		$data['liisi_private'] = (isset($this->request->post['liisi_private']) ? $this->request->post['liisi_private'] : $this->config->get('liisi_private'));
		$data['liisi_private_password'] = (isset($this->request->post['liisi_private_password']) ? $this->request->post['liisi_private_password'] : $this->config->get('liisi_private_password'));
		$data['liisi_snd_id'] = (isset($this->request->post['liisi_snd_id']) ? $this->request->post['liisi_snd_id'] : $this->config->get('liisi_snd_id'));
		$data['liisi_payment_confirm'] = (isset($this->request->post['liisi_payment_confirm']) ? $this->request->post['liisi_payment_confirm'] : $this->config->get('liisi_payment_confirm'));
		$data['liisi_status'] = (isset($this->request->post['liisi_status']) ? $this->request->post['liisi_status'] : $this->config->get('liisi_status'));

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('payment/liisi.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/liisi')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}


		return !$this->error;
	}
}
